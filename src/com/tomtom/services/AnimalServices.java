package com.tomtom.services;

import com.tomtom.domain.Animals;
import com.tomtom.domain.Humans;
import com.tomtom.repositories.*;

import java.util.Random;
import java.util.Scanner;


public class AnimalServices {
    private static Scanner scanner;
    private AnimalRepositories animalRepositories = new AnimalRepositories();
    private HumanRepositories humanRepositories = new HumanRepositories();

    public void AnimalService(Scanner scanner) {

        this.scanner = scanner;
    }
    public static void showInitialOptions() {
        System.out.println("Please select an option : "
                + "\n\t1. Managing animals"
                + "\n\t2. Managing humans"
                + "\n\t3. Quit");

        scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        switch (choice){
            case 1:
                AnimalServices.manageAnimals();
                break;
            case 2:
                AnimalServices.manageHumans();
                break;
            default:
                break;
        }
    }public static void manageAnimals() {
        System.out.println("Please select an option : "
                + "\n\t1. Show all animals"
                + "\n\t2. Add new animal"
                + "\n\t3. Search for an animal"
                + "\n\t4. Go back");
        int choice = scanner.nextInt();
        switch (choice){
            case 1:
                showAllAnimals();
                break;
            case 2:
                addNewAnimal();
                break;
            case 3:
                searchForAnimal();
                break;
            default:
                showInitialOptions();
        }
    }

    private static void searchForAnimal() {
        System.out.println("Type in the name of the animal you want to search for :");
        String name = scanner.next();
        if (name.equals("")){
            System.out.println("please enter a valid name!");
            searchForAnimal();
        }else {
            boolean doesExist = false;
            for (Animals a: AnimalRepositories.animals){
                if(a.getName().equals(name)){
                    doesExist = true;
                    System.out.println(name + "exists");
                }
            }if (doesExist != true){
                System.out.println("There is no such animal");
            }
        }
        showInitialOptions();
    }

    private static void addNewAnimal() {
        System.out.println("Adding a new animal .." + "\nPlease enter the animal's name:");
        String name = scanner.next();
        System.out.println("Does the animal have a tail? Type yes or no");
        String hasTail = scanner.next();
        boolean hasTails = false;
        if (name.equals("") || hasTail.equals("")){
            System.out.println("Please enter valid information!");
            addNewAnimal();
        }else if (hasTail.equals("yes")){
            hasTails = true;
            Animals animal = new Animals(name ,hasTails );
            AnimalRepositories.animals.add(animal);
        }else if (hasTail.equals("no")){
            Animals animal = new Animals(name ,hasTails );
            AnimalRepositories.animals.add(animal);
        }else {
            System.out.println("Wrong input , please enter valid information!");
            addNewHuman();
        }
        showInitialOptions();
    }

    private static void showAllAnimals() {
        for (Animals a : AnimalRepositories.animals){
            a.getDetails();
            System.out.println("**************");
        }
        showInitialOptions();
    }

    public static void manageHumans() {
        System.out.println("Please select an option : "
                + "\n\t1. Show all humans"
                + "\n\t2. Add new human"
                + "\n\t3. Search for a human"
                + "\n\t4. Go back");
        int choice = scanner.nextInt();
        switch (choice){
            case 1:
                showAllHumans();
                break;
            case 2:
                addNewHuman();
                break;
            case 3:
                searchForHuman();
                break;
            default:
                showInitialOptions();
        }
    }

    private static void searchForHuman() {
        System.out.println("Type in the name of the human you want to search for :");
        String name = scanner.next();
        if (name.equals("")){
            System.out.println("please enter a valid name!");
            searchForHuman();
        }else {
            boolean doesExist = false;
            for (Humans h: HumanRepositories.humans){
                if(h.getName().equals(name)){
                    doesExist = true;
                    System.out.println(name + "exists");
                }
            }if (doesExist != true){
                System.out.println("There is no such animal");
            }
        }
        showInitialOptions();
    }

    private static void addNewHuman() {
        System.out.println("Adding a new human .." + "\nPlease enter the humans name:");
        String name = scanner.next();
        System.out.println("Is the person a man ? Type yes or no");
        String isMan = scanner.next();
        boolean isManly = false;
        if (name.equals("") || isMan.equals("")){
            System.out.println("Please enter valid information!");
            addNewHuman();
        }else if (isMan.equals("yes")){
            isManly = true;
            Humans human = new Humans(name ,isManly );
            HumanRepositories.humans.add(human);
            showInitialOptions();
        }else if (isMan.equals("no")){
            Humans human = new Humans(name ,isManly );
            HumanRepositories.humans.add(human);
            showInitialOptions();
        }else {
            System.out.println("Wrong input , please enter valid information!");
            addNewHuman();
        }
        showInitialOptions();
    }

    private static void showAllHumans() {
        for (Humans h : HumanRepositories.humans){
            h.getDetails();
            System.out.println("***********");
        }
        showInitialOptions();
    }

}
