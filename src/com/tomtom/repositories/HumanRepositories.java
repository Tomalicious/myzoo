package com.tomtom.repositories;

import com.tomtom.domain.Humans;

import java.util.ArrayList;

public class HumanRepositories {
    public static ArrayList<Humans> humans = new ArrayList<Humans>();

    public  ArrayList<Humans> getHumans() {
        return humans;
    }

    public  void setHumans(ArrayList<Humans> humans) {
       HumanRepositories.humans = humans;
    }
}
