package com.tomtom.repositories;

import com.tomtom.domain.Animals;
import com.tomtom.domain.Humans;

import java.util.ArrayList;

public class AnimalRepositories {

    public static ArrayList<Animals> animals = new ArrayList<Animals>();


    public  ArrayList<Animals> getAnimals() {
        return animals;
    }

    public  void setAnimals(ArrayList<Animals> animals) {
        AnimalRepositories.animals = animals;
    }


}

