package com.tomtom.domain;

public class Humans {
    public String name;
    public boolean isMan;

    public Humans(String name, boolean isMan) {
        this.name = name;
        this.isMan = isMan;
    }

    public void getDetails(){
        System.out.println("Name : " + this.name + "\nIs this a man ? " + this.isMan);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMan() {
        return isMan;
    }

    public void setMan(boolean man) {
        isMan = man;
    }
}
