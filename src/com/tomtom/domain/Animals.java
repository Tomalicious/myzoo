package com.tomtom.domain;

public class Animals {
    public String name;
    public boolean hasTail;

    public Animals(String name, boolean hasTail) {
        this.name = name;
        this.hasTail = hasTail;
    }

    public void getDetails(){
        System.out.println("Name : " + this.name + "\nDoes this animal have a tail? :" + this.hasTail);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasTail() {
        return hasTail;
    }

    public void setHasTail(boolean hasTail) {
        this.hasTail = hasTail;
    }
}
