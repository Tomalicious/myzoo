package com.tomtom.domain;

import com.tomtom.domain.Animals;
import com.tomtom.domain.Humans;
import com.tomtom.services.AnimalServices;

import java.util.*;

public class Main {
    public  Scanner scanner = new Scanner(System.in);
    public  void main(String[] args) {

        System.out.println("Welcome to the animal kingdom");
        AnimalServices.showInitialOptions();
    }

}
